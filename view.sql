create view nodes as
select name,size,dir,null,null,null,null,"file" as type from file
union
select name,null,null,email,null,null,null,"author" as type from author
union
select subject as name,null,null,null,hash,abbrev_hash,date,"commit" as type from commits;
