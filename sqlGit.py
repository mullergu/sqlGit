#!/usr/bin/python
# -*- coding: utf-8 -*-
#  sqlGit
#
#  Copyright 2015 Guénaël Muller <contact@inkey-art.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.

import sqlite3
import os
from subprocess import Popen, PIPE
from walkdir import filtered_walk
from timeit import default_timer as timer
# use walkir instead of os.walk to be able
# to select a depth


def calctime(method):

    def calctime(*args, **kw):
        ts = timer()
        result = method(*args, **kw)
        te = timer()

        print( "\t%r : %2.2f sec" % \
              (method.__name__, te-ts))
        return result

    return calctime

class sqlGit(object):

    ''' Transform Git data into sqlite database to
    analyse these datas into program like gephi'''

    # CREATE

    def __init__(self, db):
        ''' Initialise sqlite database file'''
        self.conn = sqlite3.connect(db)

    @calctime
    def create_table(self):
        ''' create SQL tables : author, commits, file, link_author
        commit and link commit_file .'''

        c = self.conn.cursor()

        c.execute('''CREATE TABLE author(
                    name text,
                    email text,
                    primary key(name,email));''')

        c.execute('''CREATE TABLE commits(
                    subject text,
                    hash text primary key,
                    abbrev_hash text,
                    date integer);''')
        # value of dir :
        # root -> "."
        # folder -> "./dossier"
        # subfolders-> "./dossier1/dossier2"
        c.execute('''CREATE TABLE file(
                    name text,
                    size integer,
                    dir text )''')

        # Link
        c.execute('''CREATE TABLE link_author_commits(
                    author_name text,
                    author_mail text,
                    commits text
                        references commits(hash),
                    foreign key (author_name,author_mail)
                        references author(name,mail) );''')

        # TODO : chercher à voir si l'on trouve des données
        # ajout/suppressions de lignes sur le commit pour un fichier défini.
        c.execute('''CREATE TABLE link_file_commits(
                    file text,
                    dir text,
                    commits text,
                    foreign key (file,dir)
                        references file(name,dir),
                    foreign key (commits)
                        references commits(hash));''')

        # save last and first commit date for file and author
        c.execute('''CREATE TABLE date_file(
                    file text,
                    dir text,
                    deb_date integer,
                    fin_date integer,
                    foreign key (file,dir)
                        references file(name,dir)
                    );''')

        c.execute('''CREATE TABLE date_author(
                    name text,
                    email text,
                    deb_date integer,
                    fin_date integer,
                    foreign key (name,email)
                        references author(name,email)
                    );''')
        self.conn.commit()

    # INSERT
    # DATA

    def insert_all(self, path, depth):
        ''' insert all content and delete unused file'''
        os.chdir(path)
        self.insert_author()
        self.insert_commit()
        self.insert_file(depth)
        self.insert_link_A_C()
        self.insert_link_F_C()
        self.insert_dates()
        self.delete_unused_file()

    @calctime
    def insert_author(self):
        ''' insert author data into author table'''

        log = Popen("git log --format=%aN[;;|\/|;;]%aE".split(), stdout=PIPE)
        sort = Popen("sort -u".split(), stdin=log.stdout, stdout=PIPE)
        log.stdout.close()
        f = sort.stdout.read().decode('utf-8', 'replace')
        dAuthor = f.splitlines()
        c = self.conn.cursor()
        for line in dAuthor:
            data = line.split('[;;|\/|;;]')
            c.execute(
                '''INSERT INTO author(name,email)
                        values (?,?);''', data)
        self.conn.commit()

    @calctime
    def insert_commit(self):
        ''' insert commit data into commits table'''
        log = Popen(
            "git log --format=%s[;;|\/|;;]%H[;;|\/|;;]%t[;;|\/|;;]%at".split(), stdout=PIPE)
        sort = Popen("sort -u".split(), stdin=log.stdout, stdout=PIPE)
        log.stdout.close()
        f = sort.stdout.read().decode('utf-8', 'replace')
        dCommit = f.splitlines()
        c = self.conn.cursor()
        for line in dCommit:
            data = line.split('[;;|\/|;;]')
            c.execute(
                '''INSERT INTO commits(subject,hash,abbrev_hash,date)
                        values (?,?,?,?);''', data)
        self.conn.commit()

    def _get_size_dir(self,path):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size

    @calctime
    def insert_file(self,depth):
        ''' insert file data into file table, depth define how precise
        file data will be.'''
        if depth > 0:
            mdepth = depth
        else:
            mdepth = None
        c = self.conn.cursor()
        # TODO : gérer profondeur et ignorer fichier dans .git ?
        for root, dirs, files in filtered_walk(".", depth=depth):
            for fn in files:
                path = os.path.join(root, fn)
                size = os.path.getsize(path)
                data = (fn, size, root)
                c.execute(
                    '''INSERT INTO file(name,size,dir)
                                    values (?,?,?);''', data)
        for root, dirs, files in filtered_walk(".",
                                               depth=depth,
                                               min_depth=mdepth):
            for d in dirs:
                path = os.path.join(root, d)
                size = self._get_size_dir(path)
                data = (d, size, root)
                c.execute(
                    '''INSERT INTO file(name,size,dir)
                                    values (?,?,?);''', data)
        self.conn.commit()

        # LINKS
    @calctime
    def insert_link_A_C(self):
        ''' insert link data between author and commit into
        link_author_commits table'''

        log = Popen(
            'git log --pretty=format:%aN[;;|\/|;;]%aE[;;|\/|;;]%H'.split(), stdout=PIPE)
        f = log.stdout.read().decode('utf-8', 'replace')
        dLinkC_A = f.splitlines()
        c = self.conn.cursor()
        for line in dLinkC_A:
            data = line.split('[;;|\/|;;]')
            c.execute(
                '''INSERT INTO link_author_commits(author_name,
                                               author_mail,commits)
                        values (?,?,?);''', data)
        self.conn.commit()

    @calctime
    def insert_link_F_C(self):
        ''' insert link data between file and commit into
        link_author_commits table'''

        # TODO : utiliser git show ?
        c = self.conn.cursor()
        c.execute("SELECT dir,name FROM file;")
        for row in c.fetchall():
            path = os.path.join(row[0], row[1])
            cmd = 'git log --pretty=format:%H' + ' -- ' + path
            log = Popen(cmd.split(), stdout=PIPE)
            f = log.stdout.read().decode('utf-8', 'replace')
            dLinkF_C = f.splitlines()
            for commits in dLinkF_C:
                data = (row[1], row[0], commits)
                c.execute(
                    '''INSERT INTO link_file_commits(file,dir,commits)
                                    values (?,?,?);''', data)
        self.conn.commit()

    @calctime
    def insert_dates(self):

        c = self.conn.cursor()
        c.execute(
            '''INSERT INTO date_file(deb_date,fin_date,file,dir)
                    Select min(c.date),max(c.date),lfc.file,lfc.dir
                    from link_file_commits lfc join commits c
                    on c.hash=lfc.commits
                    group by lfc.file,lfc.dir;'''
        )
        c.execute(
            '''INSERT INTO date_author(deb_date,fin_date,name,email)
                    Select min(c.date),max(c.date),lac.author_name,lac.author_mail
                    from link_author_commits lac join commits c
                    on c.hash=lac.commits
                    group by lac.author_name,lac.author_mail;'''
        )
        self.conn.commit()

    @calctime
    def view_3nodestype(self):
        '''View with 3 types of nodes : author,commit,file'''
        c = self.conn.cursor()
        c.execute(
            '''create table nodes as
        select f.dir || f.name as id ,
               f.size as taille,
               f.dir as dir,
               "" as name,
               "" as email,
               "" as subject,
               "" as abbrev_hash,
               datetime(df.deb_date, 'unixepoch') as deb_date,
               datetime(df.fin_date,'unixepoch') as fin_date,
               "fichier" as type,
               f.dir || f.name as label
        from file f join date_file df
        where f.dir=df.dir
        and f.name=df.file
        union
        select a.name || a.email as id,
               "" as taille,
               "" as dir,
               a.name as name,
               a.email as email,
               "" as subject,
               "" as abbrev_hash,
               datetime(da.deb_date, 'unixepoch') as deb_date,
               datetime(da.fin_date,'unixepoch') as fin_date,
                "author" as type,
                a.name as label
        from author a join date_author da
        where a.email=da.email
        and a.name=da.name
        union
        select hash as id,
               "" as taille,
               "" as dir,
               "" as name,
               "" as email,
               subject as subject,
               abbrev_hash as abbrev_hash,
               datetime(date, 'unixepoch') as date_deb,
               datetime(date, 'unixepoch') as date_fin,
               "commit" as type,
               abbrev_hash as label
        from commits;
        ''')
        c.execute(
            '''create table edges as
        select author_name || author_mail as source,
               commits as target,
               author_name,
               "" as dir
        from link_author_commits
        union
        select dir || file as source,
               commits as target,
               "" as author_name,
               dir
        from link_file_commits;
        ''')
        self.conn.commit()

    @calctime
    def view_2nodestype(self):
        '''View with 2 types of nodes : author,file'''
        c = self.conn.cursor()

        c.execute('''create table nodes as
        select f.dir || f.name as id ,
               f.size as taille,
               f.dir as dir,
               "" as name,
               "" as email,
               datetime(df.deb_date, 'unixepoch') as deb_date,
               datetime(df.fin_date,'unixepoch') as fin_date,
               "fichier" as type,
               f.dir || f.name as label
        from file f join date_file df
        where f.dir=df.dir
        and f.name=df.file
        union
        select a.name || a.email as id,
               "" as taille,
               "" as dir,
               a.name as name,
               a.email as email,
               datetime(da.deb_date, 'unixepoch') as deb_date,
               datetime(da.fin_date,'unixepoch') as fin_date,
                "author" as type,
                a.name as label
        from author a join date_author da
        where a.email=da.email
        and a.name=da.name''')

        c.execute(''' create table edges as
        select
            lac.author_name || lac.author_mail as source,
            lfc.dir || lfc.file as target,
            count(distinct(c.hash)) as nb_commit,
            count(distinct(c.hash)) as weight,
            datetime(min(c.date),'unixepoch') as date_first_commit,
            datetime(max(c.date),'unixepoch') as date_last_commit
        from
        link_author_commits lac
        join
        commits c
        on lac.commits = c.hash
        join
        link_file_commits lfc
        on c.hash = lfc.commits
        group by lac.author_name || lac.author_mail,lfc.dir || lfc.file;
        ''')
        self.conn.commit()

    @calctime
    def view_files(self):
        '''View of relation between files'''
        c = self.conn.cursor()

        c.execute('''create table nodes as
        select f.dir || f.name as id ,
               f.size as taille,
               f.dir as dir,
               datetime(df.deb_date, 'unixepoch') as deb_date,
               datetime(df.fin_date,'unixepoch') as fin_date,
               f.dir || f.name as label
        from file f join date_file df
        where f.dir=df.dir
        and f.name=df.file;''')

        c.execute('''
        create table edges as
        select f1.file AS source , f2.file  AS target,
        count(distinct(f1.author)) as nbAuthors,
        count(distinct(f1.author)) as weight

        from
        (
            (select
                a.author_name || a.author_mail as author,
                f.dir || f.file as file
                from link_file_commits f, link_author_commits a
                where ( f.commits=a.commits)
            )as f1
            join
            (select
                a.author_name || a.author_mail as author,
                f.dir || f.file as file
                from link_file_commits f, link_author_commits a
                where ( f.commits=a.commits)
            )as f2
            on f1.author = f2.author
            and f1.file != f2.file
        )
        group by  f1.file , f2.file;''')
        self.conn.commit()

    @calctime
    def view_authors(self):
        '''View of relation between authors'''
        c = self.conn.cursor()
        c.execute('''
        create table nodes as
               select distinct( a.name || a.email) as id,
               a.name as name,
               a.email as email,
               a.name as label,
               datetime(da.deb_date, 'unixepoch') as deb_date,
               datetime(da.fin_date,'unixepoch') as fin_date
        from author a join date_author da
        where a.email=da.email
        and a.name=da.name;''')

        c.execute('''
        create table edges as
        select a1.author AS source , a2.author AS target,
        count(a1.file) as nbfile,
        count(a1.file) as weight,
        a1.file as label
        from
        (
            (select
                a.author_name || a.author_mail as author,
                f.dir || f.file as file
                from link_file_commits f, link_author_commits a
                where ( f.commits=a.commits)
            )as a1
            join
            (select
                a.author_name || a.author_mail as author,
                f.dir || f.file as file
                from link_file_commits f, link_author_commits a
                where ( f.commits=a.commits)
            )as a2
            on a1.file = a2.file
            and a1.author != a2.author
        )
        group by a1.author,a2.author
        ;''')
        self.conn.commit()

    @calctime
    def view_file_commits(self):
        pass

    @calctime
    def view_authors_commits():
        pass
    # CLEAN

    @calctime
    def delete_unused_file(self):
        ''' delete unused file (who are not linked to any commit.)'''

        c = self.conn.cursor()
        c.execute(
            '''select f.name,f.dir
                from file  f left join link_file_commits l
                on f.name=l.file and f.dir=l.dir
                where l.commits is null;''')
        for row in c.fetchall():
            c.execute(
                '''DELETE from file
                where name="''' + row[0] +
                '" and dir="' + row[1] + '";')
        self.conn.commit()

    @calctime
    def clean_DB(self):
        ''' drop alls tables'''

        c = self.conn.cursor()
        listTable = ('author', 'commits', 'file', 'link_author_commits',
                     'link_file_commits','date_author','date_file')
        for table in listTable:
            c.execute('DROP TABLE IF EXISTS ' + table + ' ;')
        self.conn.commit()

    @calctime
    def dropView(self):
        ''' drop current view'''

        c = self.conn.cursor()
        listTable = ('nodes', 'edges')
        for table in listTable:
            c.execute('DROP TABLE IF EXISTS ' + table + ' ;')
        self.conn.commit()

# autotest
if __name__ == '__main__':
    ts = timer()
    print("Creating database…")
    db = sqlGit('self.db')
    db.clean_DB()
    db.dropView()
    db.create_table()
    print("inserting data…")
    db.insert_all('.', 0)
    print("Creating view…")
    db.view_authors()
    te = timer()
    print("database ready : %2.2f sec" % (te-ts))
