sqlGit
=======
Small python program to create an sql database with data from git about
author,commit and files in order to be able to display them as a graph
in programs like Gephi.

## Context ##

Program reated for a projet in IC05 class at UTC (Université Téchnologique
de Compiègne, France) .

## Contributors ##
- Guénaël Muller

## Requirement ##
* Python, preferably python 3

## License ##
Code-source is under GPLv3 license.
git is a trademark of …
