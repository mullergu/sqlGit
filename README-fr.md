sqlGit
=======
SqlGit est un petit programme écrit en python dont le but est de créer
une base de données sql à partir des informations contenu dans un dépôt
git au sujet des auteurs, des commits et des fichiers concernés.
Le but étant de pouvoir utiliser ces informations afin de pouvoir les
étudier sous forme de graphes dans des programmes comme Gephi.

## Contexte ##

Ce programme à été réaliser dans le cadre d'un projet de l'enseignement
IC05 à l'UTC (Université Technologique de Compiègne,France).

## Contributors ##
- Guénaël Muller

## Pré-requis ##
* python, de préférence python 3.

## License ##
Le code source est sous la licence GPL version 3.
git est un marque déposée de …
